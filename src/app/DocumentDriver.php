<?php

namespace Nodopiano\DocumentsGenerator;

interface DocumentDriver
{
    public function setDocumentTitle($title);
    public function getStorageFolder();
    public function getInformations($text);
    public function createDocument($folder, $info, $context = []);
    public function deleteFile($percorso);
}
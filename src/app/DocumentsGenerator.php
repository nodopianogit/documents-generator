<?php

namespace Nodopiano\DocumentsGenerator;

use setasign\Fpdi\Fpdi;
use Smalot\PdfParser\Parser;
use Illuminate\Support\Facades\Storage;


class DocumentsGenerator
{
    protected $driver;
    protected $file;
    protected $folder;
    protected $percorso_file;

    public function loadDriver($type)
    {
        $class = config('documents-generator.drivers.'.$type);
        $this->driver = new $class;
    }

    public function fromZip($title, $file, $context = [])
    {
        $this->file = $file;
        $this->folder = $this->getFolder();
        $this->loadDriver('zip');
        $this->driver->setDocumentTitle($title);

        $results = array('done' => [], 'error' => []);
        $zip = new \ZipArchive;
        if ($zip->open($this->driver->getStorageFolder() . $this->file) === true) {

            for($i = 0; $i < $zip->numFiles; $i++) {
                $nome_file = $zip->getNameIndex($i);
                $info = $this->driver->getInformations($nome_file);
                $path = $this->driver->createDocument($this->folder, $info, $context);
                if ($path === false) {
                    $results['error'][] = $nome_file;
                }
                else {
                    $zip->extractTo($this->driver->getStorageFolder() . $path , array($nome_file));
                    $results['done'][] = $nome_file;
                }
            }

            $zip->close();

        }
        $this->driver->deleteFile($this->file);
        return $results;

    }

    public function fromPdf($filePath, $context = [])
    {
        $this->file = $filePath;
        $this->folder = $this->getFolder();
        $this->loadDriver('pdf');
        // $this->driver->setDocumentTitle($title);

        $results = array('done' => [], 'error' => []);

        $parser = new Parser();
        $pdf = new Fpdi('l');
        try {
            $n_of_pages = $pdf->setSourceFile($this->driver->getStorageFolder() . $filePath);
        } catch (\setasign\Fpdi\PdfParser\PdfParserException $e) {
            $results['error'][] = 'Non è stato possibile estrarre i file, controllare il documento inserito.';
            return $results;
        }

        for ($i=1; $i <= $n_of_pages; $i++) { 
            $pdf = new Fpdi('l');
            $pdf->setSourceFile($this->driver->getStorageFolder() . $this->file);
            $tpl = $pdf->importPage($i);
            $pdf->addPage();
            $pdf->useTemplate($tpl);

            $pageText = $parser->parseContent($pdf->Output('S'));
            //$info = $this->driver->getInformations($pageText->getText());

            $document = $this->driver->createDocument($this->folder, $pageText->getText(), $context);

            if ($document->hasError) {
                $results['error'][] = 'pagina '. $i . ': ' . $document->page;
            }
            else {
                $pdf->Output('F', $this->driver->getStorageFolder() . $document->path);
                $results['done'][] = $document->page;
            }
        }
        $this->driver->deleteFile($this->file);
        return $results;
    }

    private function getFolder() 
    {
        $percorso_file = explode('/', $this->file);
        unset($percorso_file[count($percorso_file) - 1]);
        $folderPath = implode('/', $percorso_file);

        return $folderPath;
    }
}
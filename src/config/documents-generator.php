<?php

return [

    /* ---------------------------
        Driver documenti
            use a class instance: 'driver' => App\DriverDocumenti::class
    ---------------------------- */

    'drivers' => null,
    
    /* ---------------------------
        File extension allowded
    ---------------------------- */

    'allowed' => ['pdf'],

    /* ---------------------------
        Extractable categories
    ---------------------------- */

    'extractable' => [
        'rette' => 'rette',
        '- Cedolino Laser' => 'cedolini paga'
    ]
];